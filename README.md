# Projet B1 2023

> *Sujet du projet système et réseau des B1 2022/2023.*

Le but du projet est de vous permettre de réutiliser le skill que vous avez acquéri en cours de systèmes (Linux) et réseaux, dans le cadre d'un projet libre.

**On se rapproche d'un cas réel, et vous allez héberger vous-mêmes une solution de votre choix, réutilisant le savoir acquis  dans l'année au sein d'un contexte concret.**

> La logique derrière tout ça est que peu importe votre profil (admin, sys, réseau, sécu, dév, etc) c'est un skill élémentaire de tout technicien que de savoir héberger un ptit service.

## Sujet

**Le choix du sujet est laissé libre aux étudiants.**

➜ L'idée est la suivante :

- former des groupes de 2 à 4
  - de 2 à 4 ça veut dire que vous pouvez être 2 ou 3 ou 4
  - genre 2
  - ou 3
  - ou 4
  - genre du coup pas 1
  - ni 0 d'ailleurs
  - ni + de 4
  - je vous aide avec l'arithmétique parce que je vois souvent que c'est pas simple
- choisir une technologie une solution (libre et open source, il y a aura de bonnes docs) utile, que vous aimeriez savoir monter
  - voyez larges : serveur de stream, serveur de jeu, etc etc
- mettre en place la solution dans un environnement virtuel
  - des VMs dans votre PC
  - des VMs mis à disposition par l'école
- agrémenter la solution avec des services additionels, pour augmenter son niveau de qualité ou de sécurité
  - mise en place de monitoring, backup, déploiement automatisé avec un script, etc.


➜ au minimum donc :

- 2 machines qui fonctionnent ensemble
- le minimum syndical de bonnes pratiques mis en place
  - un user chacun
  - accès aux droits de `root` *via* `sudo` pour chacun
  - pas de connexion par mot de passe, uniquement par clé SSH
  - fail2ban installé et actif sur toutes les machines
- 3 éléments de sécurité additionels
  - les possibilités sont énormes, n'hésitez pas à me contacter pour avoir des pistes à ce sujet

> *C'est le même principe que le TP NextCloud, mais plutôt que NextCloud vous choisissez vous-même la solution à héberger. Ici le mot "solution" peut être remplacer par "application", "logiciel", ce que vous voulez. On appelle ça une solution, car ça vient répondre à un besoin, résoudre une problématique.*

➜ **Vous pourrez trouver ici une liste non-exhaustive des projets intéressants que vous pouvez utiliser : https://github.com/awesome-selfhosted/awesome-selfhosted**

➜ **Dit avec d'autres mots, concrètement :**

- vous allez héberger vous-même un service
- je vous encourage très fort à louer un ptit serveur en ligne chez OVH ou Scaleway, pour être dans une situation réelle et avoir un vrai serveur public que vous administrez
- vous pouvez aussi faire ça dans des VMs (locales sur votre PC ou mises à disposition par l'école)

## Rendu attendu

### Oral

**La dernière séance sera consacrée à un oral** où vous présenterez votre projet :

- présentation du besoin
- présentation de votre solution (et en quoi elle répond au besoin)
- démonstration

La présentation sera notée sur 4 critères, chacun sur 5 points :

- pertinence de la problématique
  - est-ce que le besoin est actuel ? est-ce que vous y avez correctement répondu ?
- challenge recherché
  - est-ce que c'était enfantin à faire ou y'a du job et du challenge derrière votre projet
- qualité de la démo
  - est-ce que la démo répond concrètement et techniquement au besoin ?
- qualité de la prés
  - est-ce que c'était agréable de vous écouter ? est-ce qu'il y avait du flow ?

### Ecrit

L'écrit sera à rendre quelques jours après l'oral (les dates varieront suivant votre promo, elles vous seront communiquées en temps et en heure).

**L'écrit est un dépôt git** public qui contient au moins un `README.md` qui explique :

- des prérequis (de votre choix)
- des instructions d'installation
- des instructions d'accès

> Par exemple : indiquer de partir d'une Rocky9 avec une IP privée en prérequis, installer tel tel paquet et modifier telle conf, puis accéder à telle adresse pour consommer la solution.
